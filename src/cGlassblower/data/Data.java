package cGlassblower.data;

import org.rspeer.runetek.adapter.component.Item;
import java.util.function.Predicate;

public class Data {

    public static final String formatValue(final long l) {
        return (l > 1_000_000) ? String.format("%.2fm", ((double) l / 1_000_000))
                : (l > 1000) ? String.format("%.1fk", ((double) l / 1000)) : l + "";
    }

    public static long lastAnimation = 0;

    public static int moltenGlassCount = 1;

    public static final Predicate<Item> BANK_ITEMS = x-> x.getName().equals(MoltenGlassObjects.BEER_GLASS.getName()) || x.getName().equals(MoltenGlassObjects.CANDLE_LANTERN.getName())
            || x.getName().equals(MoltenGlassObjects.OIL_LAMP.getName()) || x.getName().equals(MoltenGlassObjects.VIAL.getName()) || x.getName().equals(MoltenGlassObjects.FISHBOWL.getName())
            || x.getName().equals(MoltenGlassObjects.UNPOWERED_STAFF_ORB.getName()) || x.getName().equals(MoltenGlassObjects.LANTERN_LENS.getName()) || x.getName().equals(MoltenGlassObjects.LIGHT_ORB.getName());

    public enum MoltenGlassObjects{

        BEER_GLASS("Beer glass", 0),
        CANDLE_LANTERN("Empty candle lantern", 1),
        OIL_LAMP("Empty oil lamp", 2),
        VIAL("Vial",3),
        FISHBOWL("Empty fishbowl",4),
        UNPOWERED_STAFF_ORB("Unpowered orb", 5),
        LANTERN_LENS("Lantern lens", 6),
        LIGHT_ORB("Empty light orb",7);

        private String name;
        private int index;

        MoltenGlassObjects(String name, int index){
            this.name = name;
            this.index = index;
        }

        public String getName(){
            return name;
        }

        public int getIndex(){
            return index;
        }
    }

    public static int startLevel = 0;
    public static int startXp = 0;


}
