package cGlassblower.tracker;

import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

public class Experience {

    public final int gainedLvl(Skill skill, int startLvl) {
        int currentSkillLevel = Skills.getLevel(skill);
        return currentSkillLevel - startLvl;
    }

}
